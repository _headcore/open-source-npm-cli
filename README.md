# Script command
hcr

# Commands
create
controller

# Options for Create
type: type of project (api or lib)
directory: folder name (optional)
packageName: package name <any>
packages: packages to install after structure creation (optional)

# Example
hcr create --type <api or lib> --directory <myDirectory> --packageName <myPackageName>
hcr create --type api --directory newProject --packageName @mproject/my 
hcr create --type lib --directory newProject2 --packageName @mproject/my2

hcr controller <controllerName> <controllerPrefix> <controllerVersion>
hcr controller User user 1


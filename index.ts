#!/usr/bin/env node
import program = require('commander');
import { exec, execSync } from 'child_process';
import fs = require('fs');
import { 
    packageJson, tsconfigJson, apiConfig, 
    list, cliConfigTemplate, localEnvironmentContent, gitignore, 
    vsCodeLaunch, vsCodeTasks
} from './src/config';
import * as path from 'path';
import { controllerTemplate } from '@headcore/express-extensions';
const cliConfigsFileName = 'app.config.json';

interface CLIConfigs {
    controllersDirectory: string;
    routesDirectory: string;
    loadControllersScript: string;
}

program
  .command('create')
  .option('-p, --packages <items>', 'Packages to includes in creation', list)
  .option('-t, --type <type>', 'Package type')
  .option('-d, --directory <directoryName>', 'Package directory')
  .option('-f, --packageName <packageName>', 'Package name')
  .option('-d, --debug <debugIDE>', 'IDE Debug')
  .action(function (cmd) {

        const type = cmd.type.toLowerCase(); 
        const packageName = cmd.packageName; 
        const packageRootFolder = `./${cmd.directory ? cmd.directory : ''}`
        const directoryExists = fs.existsSync(packageRootFolder);
        const packages: string[] = cmd.packages ? cmd.packages : [];
        let debug: string = cmd.debug ? cmd.debug : '';

        if(cmd.directory && directoryExists) {        
            console.log('A Directory with the specified name already exists')
            return;
        }

        if(cmd.directory) {
            fs.mkdirSync(`${packageRootFolder}`);
            console.log('Directory created.');
        }

        // In commmon
        fs.mkdirSync(`${packageRootFolder}/src`);
        console.log('Source dir created.');
        fs.writeFileSync(`${packageRootFolder}/package.json`, packageJson(type, packageName));
        console.log('package.json created.');
        fs.writeFileSync(`${packageRootFolder}/tsconfig.json`, tsconfigJson());
        console.log('tsconfig.json created.');
        const buffer = execSync(`cd ${packageRootFolder} && npm install typescript --save-dev`);
        process.stdout.write(buffer);

        // Install packages
        packages.forEach((pack) => { 
            const buffer = execSync(`cd ${packageRootFolder} && npm install ${pack} --save`) 
            process.stdout.write(buffer);
        });
    
        // Actions to specified command
        switch(type) {
            case 'api':     

                exec(`cd ${packageRootFolder} && npm install @headcore/express-extensions --save`).stdout.pipe(process.stdout);

                // Root
                fs.writeFileSync(`${packageRootFolder}/index.ts`, `import './src/startup';`);
                fs.writeFileSync(`${packageRootFolder}/src/startup.ts`, `${apiConfig()}`);

                // Controllers
                fs.mkdirSync(`${packageRootFolder}/src/controllers`);

                // Environments
                fs.writeFileSync(`${packageRootFolder}/dev.env`, `${localEnvironmentContent()}`);

                // Template
                fs.writeFileSync(path.resolve(process.cwd(), packageRootFolder, cliConfigsFileName), cliConfigTemplate());

                // gitignore
                fs.writeFileSync(`${packageRootFolder}/.gitignore`, gitignore());

                if(debug.toUpperCase() === 'VSCODE') {
                    const vscodeFolder = '.vscode';
                    fs.mkdirSync(`${packageRootFolder}/${vscodeFolder}`);
                    fs.writeFileSync(`${packageRootFolder}/${vscodeFolder}/launch.json`, vsCodeLaunch());
                    fs.writeFileSync(`${packageRootFolder}/${vscodeFolder}/tasks.json`, vsCodeTasks());
                }

                break;

            case 'lib':

                fs.writeFileSync(`${packageRootFolder}/index.ts`, `export * from './src'`);

                break;
        }
  });

program
    .command('controller <name> <version>')
    .action(function (name: string, version, cmd) {
    
        const isInvalid = new RegExp(/[^A-Za-z0-9]+/g);
        if(isInvalid.test(name)) {
            console.error(`Invalid controller name!`);
            return;
        }

        const prefix = getControllerPrefix(name);
        console.log(prefix);
        const cliConfigs = getCLIConfigs();
        const versionDirectory = `./${cliConfigs.controllersDirectory}/v${version}`;
        console.log(`Controller destination directory: ${versionDirectory}`);

        const directoryExists = fs.existsSync(versionDirectory);
        if(!directoryExists) fs.mkdirSync(versionDirectory);

        const controllerPath = path.resolve(versionDirectory, `${prefix}.controller.ts`);
        if(fs.existsSync(controllerPath)) {
            return console.error(`${name}Controller already exists!`) ;
        }

        // Create controller
        fs.writeFileSync(controllerPath, controllerTemplate(name, version, prefix));
    });

program.parse(process.argv);

function getControllerPrefix(controllerName: string) {
    
    let prefix = '';
    let position = 0;

    for (const character of controllerName) {
        if(character === character.toUpperCase() && position > 0) {
            prefix += '-';
        }
        prefix += character.toLowerCase();
        position++;
    }

    return prefix;
}

function getCLIConfigs() { 

    const rootDirectory = process.cwd(); 
    const cliConfigsPath = path.resolve(rootDirectory, cliConfigsFileName);
    if(!fs.existsSync(cliConfigsPath)) {
        console.log('CLI configs not found!'); 
        process.exit(); 
    }

    const cliConfigs = require(cliConfigsPath) as CLIConfigs;
    console.log('CLI configs loaded!');
    return cliConfigs;
}